package com.system.exam;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ValidationDate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean b = getValidationDate("20200315");
		
		System.out.println(">>> " + b);
	}
	
	/***
	 * 날짜 유효성 검사
	 * @param dt String date
	 * @return true/false
	 */
	public static boolean getValidationDate(String checkDate) {
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		
		// Specify whether or not date/time parsing is to be lenient.
		formatter.setLenient(false);
		
		try {
			formatter.parse(checkDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return false;
		}
		
		return true;
		
	}

}
