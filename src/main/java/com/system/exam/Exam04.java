package com.system.exam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

public class Exam04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String[] arr = new String[] {"1", "2", "3"};

		/***
		 *  java.util.Arrays.stream(String[] array, int startInclusive, int endExclusive)


			Returns a sequential Stream with the specified range of the specified array as its source.
			
			Type Parameters:
					<T> the type of the array elements
			Parameters:
					array the array, assumed to be unmodified during use
					startInclusive the first index to cover, inclusive
					endExclusive index immediately past the last index to cover
			Returns:
			a Stream for the array range

		 */

		/***
		 * 
		 */		
		
		{
			Stream<String> streamOfArrayPart = Arrays.stream(arr, 0, 2);
	
			streamOfArrayPart.forEach(action -> {
				System.out.println(action.toString());
			});
		}

		/***
		 * 
		 */
		{
			Stream<String> buildStream = Stream.<String>builder()
					.add("Eric1")
					.add("Eric2")
					.add("Eric3")
					.add("Eric4")
					.build();

			buildStream.forEach(action -> {
				System.out.println(action.toString());
			});

			System.out.println(buildStream.toString());
		}

		/***
		 * 
		 */		
		{
			Stream<Integer> iteratedStream = Stream.iterate(30, n -> n + 2).limit(5);
			iteratedStream.forEach(action -> {
				System.out.println(action.toString());
			});
		}

		/***
		 * 
		 */
		{
			List<Students> studentsList = new ArrayList<>();

			Students students = Students.builder().Name("jangwon").Kor(20).Eng(30).Math(45).build();

			studentsList.add(students);

			studentsList.stream()
					.flatMapToInt(s -> IntStream.of(s.getEng(), s.getKor(), s.getMath())).average()
					.ifPresent(avg -> System.out.println("Average: " + Math.round(avg * 100) / 100.0));
		}			
                   		
		

		/***
		 * 
		 */		
		{	
			List<Students> studentsList = new ArrayList<>();
			
			Students students = Students.builder().Name("jangwon").Kor(20).Eng(30).Math(45).build();
			studentsList.add(students);
			Students students1 = Students.builder().Name("wonjang").Kor(425).Eng(321).Math(221).build();
			studentsList.add(students1);
			
			studentsList.forEach(s -> {
				System.out.println("> " + s.getName());
			});
		}
		
		/***
		 * 
		 */
		ArrayList<Integer> ali = (ArrayList<Integer>) IntStream.of(14, 11, 20, 39, 23)
			.sorted()
			.boxed()
			.collect(Collectors.toList());
		
		System.out.println(ali.toString());
		
		
		/***
		 * 
		 */
		ArrayList<String> als = (ArrayList<String>) Stream.of("24", "11", "20", "39", "23")
			.sorted()
			.collect(Collectors.toList());
		
		System.out.println(als.toString());
	}

}

@Getter @Setter @ToString
class Students{
	private String Name;
	private int Kor;
	private int Eng;
	private int Math;
	
	@Builder
	Students(String Name, int Kor, int Eng, int Math){
		this.Name = Name;
		this.Kor = Kor;
		this.Eng = Eng;
		this.Math = Math;
		
	}
}



