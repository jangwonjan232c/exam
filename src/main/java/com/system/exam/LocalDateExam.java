package com.system.exam;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LocalDateExam {

	static Logger logger = LoggerFactory.getLogger(LocalDateExam.class);
	
	public static void main(String[] args) {
		
				 		
		// TODO Auto-generated method stub		
		LocalDate now = LocalDate.now();
		logger.debug("now: " + now);
		
		LocalDateTime ldt = LocalDateTime.of(2020, 3, 15, 0, 0 ,0);		
		logger.debug("Local Date Time: " + ldt);		
		
		LocalDate ld = LocalDate.of(2020, 3, 15);		
		logger.debug("Local Date: " + ld);

		LocalDateTime d = LocalDateTime.parse("2016-10-31 23:59:59", 
				DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));		
		logger.debug("d: " + d);
		
		logger.debug("Hello World!!!");
		
	}

}
