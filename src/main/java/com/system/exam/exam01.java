package com.system.exam;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.stream.IntStream;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

public class exam01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("### Dto Builder Test ###");
		
/*		Dto dto = Dto.builder()
				.KRW(new BigDecimal("12"))
				.build();
		
		System.out.println(">>>>>>" + dto.getKRW());
		
		IntStream.range(1, 10).forEach(i -> System.out.println(i));
		IntStream.rangeClosed(1,10).forEach(i -> System.out.println(i));
*/		
		
		
		BigDecimal bi = divideDecimal(new BigInteger("1000000000"), new BigInteger("8"));
		System.out.println(">>>>>>" + bi.toPlainString());

		BigInteger ib = new BigInteger("-1");
		
		if(ib.compareTo(BigInteger.ZERO) < 0) {
			System.out.println(">>>>>> " + ib);
		}
				
		exec((i,j)->{return i*j;});
		
	}
	
/*	@Setter @Getter @ToString
	public static class Dto{
		private String KRW;
		
		@Builder
		public Dto(BigDecimal KRW){
			this.KRW = KRW.toString();
		}
		
	}*/
	
	
    public static BigDecimal divideDecimal(BigInteger number, BigInteger decimal) {
        if (number != null && decimal != null) {
            return new BigDecimal(number).divide(BigDecimal.TEN.pow(decimal.intValue()));
        }
        return null;
    }
    
    public static void exec(Compare com) {
    	int a = 50;
    	int b = 20;
    	int value = com.compareTo(a, b);
    	System.out.println("Value is " + value);
    	
    }
}	


@FunctionalInterface
interface Compare{
	int compareTo(int a, int b);
}


