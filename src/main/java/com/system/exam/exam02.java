package com.system.exam;

import java.util.Optional;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

public class exam02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		exec((a,b,c)->{ return a+b+c;});
		
		
		//House h = new House();
		
		//System.out.println(h.getOnwer().getName());
		//System.out.println(h.getAddress().getAddress());
		
		//Optional<Object> opt = Optional.of(h);
		//opt.ifPresent(address -> System.out.println(opt));
		
		//Person p = new Person();
		//p.setName(null);
		//h.setOnwer(p);
		
		//Address a = new Address();
		//a.setAddress("Seoul");
		//h.setAddress(a);
		
		//Person p = Person.builder().name(null).age(18).build();
		Person p = new Person();
		Optional<Person> otp = Optional.of(p);		
		otp.ifPresent(x->{System.out.println(x.getName()); });
		
/*		Optional<Person> p = Optional.empty();
		if(p.isPresent()) {
			System.out.println("present");
		}else{
			
			System.out.println("empty");
		}
*/		
		
	}
	
	public static void exec(StringAdd sa) {
		String a = "Hello";
		String b = "World";
		String c = "!!!";
		String result = sa.stringAdd(a, b, c);
		System.out.println(result);
	}

}

@FunctionalInterface
interface StringAdd{	
	String stringAdd(String strA, String strB, String  strC); 
}

@Getter @Setter @ToString
class Person{
	private String name;
	private int age;
	
/*	@Builder
	Person(String name, int age){
		this.name = name;
		this.age = age;
	}*/
}

@Getter @Setter @ToString
class Address{
	private String address;
}

@Getter @Setter @ToString
class House{
	private Person onwer;
	private Address address;
}
