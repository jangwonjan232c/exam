package com.system.exam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

public class exam03 {

	public static void main(String[] args) throws ParseException {

		String from = "2018-03-15";
		String to = "2020-03-24";
		String period = PeriodType.Period.YEAR.toString();
	
		Search s = Search.builder().from(from).to(to).period(period).build();
		
		List<DayMap> dayMapList = new ArrayList<>();
		
		switch (s.getPeriod()) {
		case "DAY":
			break;
		case "WEEK":
			dayMapList = getWeekDayMap(s, period);
			break;
		case "MONTH":
			dayMapList = getWeekDayMap(s, period);
			//dayMapList = getMonthDayMap(s);
			break;
		case "YEAR":
			dayMapList = getWeekDayMap(s, period);
			//dayMapList = getYearDayMap(s);
			break;
	
		default:
			break;
		}	

		System.out.println("DayMap is " + dayMapList);				
		System.out.println("###################################");
		// 확인
		for (DayMap d : dayMapList) {
			System.out.println("Interval: " + d.getDay() + ", Search Date: " + d.getDaySearch());			
		}		
		
	}

	/***
	 * Day Map 생성
	 * @param s
	 * @return
	 * @throws ParseException 
	 */
	public static List<DayMap> getWeekDayMap(Search s, String dayType) throws ParseException{
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
		Date fromDate = formatter.parse(s.getFrom());
		Date toDate = formatter.parse(s.getTo());
		
		Date startOfWeek = null;
		Date lastDayOfTheWeek= null;
		
		List<DayMap> dayMapList = new ArrayList<>();
		DayMap dayMap = null;
		while (fromDate.compareTo(toDate)==-1) {
			
			if(dayType.equals("WEEK")){
				startOfWeek = getCurSundaySaturday(fromDate,"sunday"); // 주초(일요일) 구하기
				lastDayOfTheWeek = getCurSundaySaturday(fromDate, "saturday"); // 주말(토요일) 구하기
			}else if(dayType.equals("MONTH")){
				startOfWeek = formatter.parse(getCurFirstDayLastDayOfMonth(fromDate,"firstday")); // 월초 구하기
				lastDayOfTheWeek = formatter.parse(getCurFirstDayLastDayOfMonth(fromDate,"")); // 월말 구하기
			}else if(dayType.equals("YEAR")){
				startOfWeek = formatter.parse(getCurStartDayLastDayOfYear(fromDate, "firstday")); // 년초 구하기
				lastDayOfTheWeek = formatter.parse(getCurStartDayLastDayOfYear(fromDate, "lastday")); // 년초 구하기
			}
			
			// 맵에 저장(구간, 조회일자)
			dayMap = DayMap.builder()
					.day(formatter.format(startOfWeek).concat(" ~ ").concat(formatter.format(lastDayOfTheWeek)))
					.daySearch(formatter.format(lastDayOfTheWeek))
					.build();
			
			fromDate = getNextDate(lastDayOfTheWeek); // 다음 일자 구하기
			if(fromDate.compareTo(toDate) == 1) {
				// 마지막 구간 맵에 저장 하고 루프 종료
				dayMap = DayMap.builder()
						.day(formatter.format(startOfWeek).concat(" ~ ").concat(formatter.format(toDate)))
						.daySearch(formatter.format(toDate))
						.build();
				dayMapList.add(dayMap);
				break;
			}
			
			dayMapList.add(dayMap);
		}

		return dayMapList;

	}
	
	/***
	 * 주초(일요일), 주말(토요일) 구하기
	 * @return
	 */
 	public static Date getCurSundaySaturday(Date day, String dayType){		
 		Calendar c = Calendar.getInstance();
 		c.setTime(day);
 		c.set(Calendar.DAY_OF_WEEK, dayType == "sunday"? Calendar.SUNDAY: Calendar.SATURDAY);
 		return c.getTime();
 	}

 	/***
 	 * 월 1일, 말일 구하기
 	 * @param day
 	 * @param dayType 1일->firstDay, 월 말일->"" 
 	 * @return
 	 */
 	public static String getCurFirstDayLastDayOfMonth(Date day, String dayType){		
 		Calendar c = Calendar.getInstance();
        c.setTime(day);
        String yyyy = Integer.toString(c.get(Calendar.YEAR));
        String mm = String.format("%02d",(c.get(Calendar.MONTH) + 1));
        String dd = dayType == "firstday"? "01" : Integer.toString(c.getActualMaximum(Calendar.DAY_OF_MONTH));
        String yyyymmdd = yyyy.concat("-").concat(mm).concat("-").concat(dd);
        return yyyymmdd;
 	}
 	
 	/***
 	 * 년도초 1일, 년도 말일 
 	 * @param day
 	 * @param dayType 년도초 1일->first, 년도 말일->"" 
 	 * @return
 	 */
 	public static String getCurStartDayLastDayOfYear(Date day, String dayType){		
 		Calendar c = Calendar.getInstance();
        c.setTime(day);
        String yyyy = Integer.toString(c.get(Calendar.YEAR));
        String yyyymmdd = yyyy
        		.concat("-")
        		.concat(dayType=="firstday"?"01":"12")
        		.concat("-")
        		.concat(dayType=="firstday"?"01":"31");
        return yyyymmdd;
 	}
 	
 	/***
 	 * 다음 일자 구하기
 	 * @param now
 	 * @return
 	 * @throws ParseException
 	 */
	public static Date getNextDate(Date day){		
		Calendar c = Calendar.getInstance();
		c.setTime(day);
		c.add(Calendar.DATE, 1); // 다음 일자 구하기
		return c.getTime();
	}
 	
}

@Getter @Setter @ToString
@NoArgsConstructor
class Search{
	private String from;
	private String to;
	private String period;
	
	@Builder
	private Search(String from, String to, String period) {
		this.from = from;
		this.to = to;
		this.period = period;
	}
}

@Getter @Setter @ToString
@NoArgsConstructor
class DayMap{
	private String day;
	private String daySearch;
	
	@Builder
	private DayMap(String day, String daySearch){
		this.day = day;
		this.daySearch = daySearch;
	}
}

@AllArgsConstructor(access = lombok.AccessLevel.PRIVATE)
class PeriodType{
	public enum Period{
		DAY("DAY"),
		WEEK("WEEK"),
		MONTH("MONTH"),
		YEAR("YEAR");
		
		@Getter
		private String type;
		
		Period(String type){
			this.type = type;
		}
	}
}

