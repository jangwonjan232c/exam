package com.system.exam;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.cliftonlabs.json_simple.JsonArray;
import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsoner;

import lombok.Getter;
import lombok.Setter;

public class JacksonCoreTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("----------serialize------------");
		  JsonObject userJObj = new JsonObject();
		  userJObj.put("ID", "happy");
		  userJObj.put("JoinDate", "20170101");
		  
		  JsonArray itemsJArr = new JsonArray();
		  
		  JsonObject itemJObj1 = new JsonObject();
		  itemJObj1.put("ItemName", "Fire Sword");
		  itemJObj1.put("Strength", 80);
		  
		  JsonObject itemJObj2 = new JsonObject();
		  itemJObj2.put("ItemName", "Thunder Bow");
		  itemJObj2.put("Strength", 90); 
		  
		  itemsJArr.add(itemJObj1);
		  itemsJArr.add(itemJObj2);
		  
		  userJObj.put("items", itemsJArr);
		  String msg = userJObj.toJson();
		  
		  System.out.println(msg);
		  System.out.println("생성된 메시지 pretty print 결과");
		  System.out.println(Jsoner.prettyPrint(msg));
		    
	
	}

	
	
	@RequestMapping(value = "/jsontest")
	public @ResponseBody
	static Object jsonTest(){
		System.out.println("-");
		Personaa person = new Personaa();
		person.setName("jangwon");
		person.setAddress("seoul secho");
		person.setAge(12);
		System.out.println("-");
	return person;

	}

}

@Getter @Setter
class Personaa{
	private String name;
	private String address;
	int age;
	
/*	@Builder
	Person(String name, String address, int age) {
		this.name = name;
		this.address = address;
		this.age = age;
	}*/
}